package org.tacbrd.rabbit.setup;

import com.rabbitmq.client.*;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Scanner;

public class RabbitExchangeSetup {


    public RabbitExchangeSetup()
    {
    }

    public static void main(String argv[])
        throws IOException
    {
        if(argv.length < 1)
        {
            System.err.println("Usage: [Host ipaddress] [Exchange Name] [Test Message] [Queue Name 1...n]");
            System.exit(1);
        }
        HOST = argv[0];
        EXCHANGE_NAME = argv[1];
        TEST_MESSAGE = argv[2];
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(HOST);
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.exchangeDeclare(EXCHANGE_NAME, "direct", true);
        String message = TEST_MESSAGE;
        for(int i = 3; i < argv.length; i++)
        {
            QUEUE_NAME = argv[i];
            BINDING_KEY = argv[i];
            channel.queueDeclare(QUEUE_NAME, true, false, false, null);
            channel.queueBind(QUEUE_NAME, EXCHANGE_NAME, BINDING_KEY);
            channel.basicPublish(EXCHANGE_NAME, BINDING_KEY, MessageProperties.PERSISTENT_TEXT_PLAIN, message.getBytes());
            System.out.println((new StringBuilder()).append(" [x] Sent '':'").append(message).append("'").toString());
        }

        Scanner clean_up_prompt = new Scanner(System.in);
        System.out.println("Delete Exchange, Queues and Bindings Just Created???[0=No, 1=Yes]");
        int answer = Integer.parseInt(clean_up_prompt.next());
        if(answer == 0)
        {
            channel.close();
            connection.close();
        } else
        {
            for(int i = 3; i < argv.length; i++)
            {
                QUEUE_NAME = argv[i];
                BINDING_KEY = argv[i];
                channel.queueUnbind(QUEUE_NAME, EXCHANGE_NAME, BINDING_KEY);
                channel.queueDelete(QUEUE_NAME);
            }

            channel.exchangeDelete(EXCHANGE_NAME);
            channel.close();
            connection.close();
        }
    }

    private static String HOST = "";
    private static String EXCHANGE_NAME = "";
    private static String QUEUE_NAME = "";
    private static String BINDING_KEY = "";
    private static String TEST_MESSAGE = "";
}
